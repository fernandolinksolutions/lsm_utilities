# -*- coding: utf-8 -*-
import argparse
import pandas as pd
import os

from classes.WS import WS
from classes.Vodafone import Vodafone
from prettyconf import config
from tqdm import tqdm


def get_information_sims(dataframe):
    ws = WS('logs/scripts.log')
    vodafone = Vodafone('logs/scripts.log')
    column = dataframe.columns.values[0]
    vodafone_column = 'msisdn' if column == 'line__msisdn' else column

    for index, row in dataframe.iterrows():
        r_sim = ws.get_sim(column, row[column])
        if r_sim:
            r_vodafone = vodafone.get_contrato(vodafone_column, r_sim[vodafone_column])
            for key in config('VODAFONE_KEYS', cast=config.list):
                r_sim[key] = r_vodafone[key]
        else:
            r_sim = {column: row[column]}

        yield r_sim


def main():
    pd.options.display.float_format = '{:.0f}'.format
    parser = argparse.ArgumentParser(
        description='Busca por iccid ou line_msisdn no lsm_ws, em /api/sims, '
                    'e no endpoint da operadora, depois gera um arquivo .csv')
    parser.add_argument(
        '-i', type=str,
        help="Arquivo para importar iccid's ou line_msisdn's")
    parser.add_argument(
        '-o', type=str,
        help="Local com nome do arquivo para exportar dados de /api/sims e da operadora")
    args = parser.parse_args()
    kwargs = {
        'file_import': args.i,
        'file_export': args.o
    }

    reader = pd.read_csv(kwargs['file_import'])
    for information in tqdm(get_information_sims(reader), total=reader.shape[0]):
        exists = os.path.isfile(kwargs['file_export'])
        with open(kwargs['file_export'], 'a') as f:
            df = pd.DataFrame([information])
            header = False if exists else True
            df.to_csv(f, sep=';', encoding='utf-8', index=False, header=header)


if __name__ == '__main__':
    main()
