# -*- coding: utf-8 -*-
import argparse
import pandas as pd
import requests

from classes.Claro import Claro
from tqdm import tqdm


def main():
    pd.options.display.float_format = '{:.0f}'.format
    parser = argparse.ArgumentParser(
        description='Busca detalhes do dispositivo por iccid no endpoint da operadora, depois gera um arquivo .csv')
    parser.add_argument(
        '-i', type=str,
        help="Arquivo para importar iccid's")
    parser.add_argument(
        '-o', type=str,
        help="Local com nome do arquivo para exportar os dados processados")
    args = parser.parse_args()
    kwargs = {
        'file_import': args.i,
        'file_export': args.o
    }

    details = list()
    claro = Claro('logs/scripts.log')
    reader = pd.read_csv(kwargs['file_import'])

    for index, row in tqdm(reader.iterrows(), total=reader.shape[0]):
        try:
            details.append(claro.get_device_details(row['iccid']))
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 404:
                details.append({'iccid': row['iccid']})

        df_result = pd.DataFrame(details)
        df_result.to_csv(kwargs['file_export'], sep='\t', encoding='utf-8', index=False)


if __name__ == '__main__':
    main()
