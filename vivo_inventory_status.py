# -*- coding: utf-8 -*-
import argparse
import pandas as pd
import requests

from classes.Vivo import Vivo
from tqdm import tqdm


def main():
    pd.options.display.float_format = '{:.0f}'.format
    parser = argparse.ArgumentParser(
        description='Busca informações do status administrativo por iccid ou msisdn no endpoint da operadora, depois'
                    'gera um arquivo .csv')
    parser.add_argument(
        '-i', type=str,
        help="Arquivo para importar iccid's ou msisdn's")
    parser.add_argument(
        '-o', type=str,
        help="Local com nome do arquivo para exportar os dados processados")
    args = parser.parse_args()
    kwargs = {
        'file_import': args.i,
        'file_export': args.o
    }

    informations = list()
    vivo = Vivo('logs/scripts.log')
    reader = pd.read_csv(kwargs['file_import'])
    column = reader.columns.values[0]

    for index, row in tqdm(reader.iterrows(), total=reader.shape[0]):
        if column == 'iccid':
            key = 'icc'
        elif column == 'line__msisdn':
            key = 'tel'
        else:
            raise ValueError('O arquivo de importação contém uma coluna inválida: {}'.format(column))

        try:
            informations.append(vivo.get_inventory_status(key, row[column]))
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 404:
                informations.append({key: row[column]})

        df_result = pd.DataFrame(informations)
        df_result.to_csv(kwargs['file_export'], sep='\t', encoding='utf-8', index=False)


if __name__ == '__main__':
    main()
