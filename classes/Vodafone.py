# -*- coding: utf-8 -*-
import logging
import requests
import time

from prettyconf import config


class Vodafone:
    def __init__(self, logfile_location):
        self.logger = self.conf_logger(logfile_location)

    @staticmethod
    def conf_logger(logfile_location):
        # create logger with 'Vodafone'
        logger = logging.getLogger('Vodafone')
        logger.setLevel('DEBUG')
        # create file handler which logs even info messages
        fh = logging.FileHandler(logfile_location)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # add the handlers to the logger
        logger.addHandler(fh)
        return logger

    def get_contrato(self, key, value):
        list_value = [{key: value}]

        try:
            self.logger.info('Consultando em /m2m/api/contrato/status. {}.'.format(list_value))
            response = requests.post(config('VODAFONE_BASE_URL') + '/m2m/api/contrato/status',
                                     json={'contratos': list_value},
                                     timeout=180,
                                     headers={
                                         'Authorization':
                                             config('VODAFONE_PB_USER') + ':' + config('VODAFONE_PB_PASS'),
                                         'Content-Type': 'application/json;charset=UTF-8',
                                     }, )
            response.raise_for_status()
            response = response.json()

            if response['resultado']['codigo'] == '28':
                self.logger.info('Contrato não pertence à Link PB.')
                response = requests.post(config('VODAFONE_BASE_URL') + '/m2m/api/contrato/status',
                                         json={'contratos': list_value},
                                         timeout=180,
                                         headers={
                                             'Authorization':
                                                 config('VODAFONE_SP_USER') + ':' + config('VODAFONE_SP_PASS'),
                                             'Content-Type': 'application/json;charset=UTF-8',
                                         }, )
                response.raise_for_status()
                response = response.json()

            if len(response):
                return response['contratos'][0]
            else:
                return None
        except requests.exceptions.Timeout:
            self.logger.error('Timeout. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_contrato(key, value)
        except requests.exceptions.ConnectionError:
            self.logger.error('Max retries exceeded. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_contrato(key, value)
        except Exception as err:
            self.logger.error('Ocorreu um erro inesperado: {}'.format(err))
            raise err
