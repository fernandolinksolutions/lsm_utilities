# -*- coding: utf-8 -*-
import logging
import requests
import time
import urllib3

from prettyconf import config

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class WS:
    def __init__(self, logfile_location):
        self.logger = self.conf_logger(logfile_location)
        self.token = self.get_token()

    @staticmethod
    def conf_logger(logfile_location):
        # create logger with 'WS'
        logger = logging.getLogger('WS')
        logger.setLevel('DEBUG')
        # create file handler which logs even info messages
        fh = logging.FileHandler(logfile_location)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # add the handlers to the logger
        logger.addHandler(fh)
        return logger

    def get_token(self):
        try:
            self.logger.info('Obtendo token para acesso.')
            request_token = requests.post(config('WS_BASE_URL') + '/token-auth/',
                                          data={'username': config('WS_USER'),
                                                'password': config('WS_PASS')},
                                          timeout=180,
                                          verify=False)
            return request_token.json()['token']
        except requests.exceptions.Timeout:
            self.logger.error('Timeout. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_token
        except requests.exceptions.ConnectionError:
            self.logger.error('Max retries exceeded. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_token
        except Exception as err:
            self.logger.error('Ocorreu um erro inesperado: {}'.format(err))
            raise err

    def refresh_token(self):
        self.logger.info('Atualizando token válido para acesso.')
        self.token = self.get_token()

    def get_sim(self, key, value):
        try:
            self.logger.info('Consultando em /api/sims. {} => {}.'.format(key, value))
            r_sim = requests.get(config('WS_BASE_URL') + '/sims',
                                 params={'filter2': '"{}":{}'.format(key, value),
                                         'format': 'json'},
                                 timeout=180,
                                 headers={'Authorization': 'JWT ' + self.token},
                                 verify=False)
            r_sim.raise_for_status()
            r_sim = r_sim.json()

            if len(r_sim):
                return r_sim[0]
            else:
                return None
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 403:
                self.logger.error('Token expirado.')
                self.refresh_token()
                return self.get_sim(key, value)

            self.logger.error('Ocorreu um erro inesperado na requisição: {}'.format(err))
            raise err
        except requests.exceptions.Timeout:
            self.logger.error('Timeout. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_sim(key, value)
        except requests.exceptions.ConnectionError:
            self.logger.error('Max retries exceeded. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_sim(key, value)
        except Exception as err:
            self.logger.error('Ocorreu um erro inesperado: {}'.format(err))
            raise err
