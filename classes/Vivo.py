# -*- coding: utf-8 -*-
import logging
import requests
import time

from prettyconf import config


class Vivo:
    def __init__(self, logfile_location):
        self.logger = self.conf_logger(logfile_location)

    @staticmethod
    def conf_logger(logfile_location):
        # create logger with 'VIVO'
        logger = logging.getLogger('VIVO')
        logger.setLevel('DEBUG')
        # create file handler which logs even info messages
        fh = logging.FileHandler(logfile_location)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # add the handlers to the logger
        logger.addHandler(fh)
        return logger

    def get_inventory_status(self, key, value, retry=None):
        id = '{}:{}'.format(key, value)
        if retry:
            cert = config('VIVO_CERT_SP')
            cert_key = config('VIVO_KEY_SP')
        else:
            cert = config('VIVO_CERT_PB')
            cert_key = config('VIVO_KEY_PB')

        try:
            self.logger.info('Consultando em /services/REST/GlobalM2M/Inventory/v6/r12/sim/{}/status.'
                             .format(id))
            response = requests.get(
                '{base_url}/services/REST/GlobalM2M/Inventory/v6/r12/sim/{id}/status'.format(
                    base_url=config('VIVO_BASE_URL'), id=id),
                cert=(cert, cert_key),
                timeout=180)
            response.raise_for_status()
            response = response.json()

            return response['statusDetailData']
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 403:
                if retry:
                    self.logger.error('Certificado e chave inválida: {}'.format(err))
                    raise err
                else:
                    self.logger.info('Contrato não pertence à Link PB.')
                    return self.get_inventory_status(key, value, True)
            elif err.response.status_code == 404:
                self.logger.error('O {} especificado não foi localizado: {}'.format(key, id))
                raise err

            self.logger.error('Ocorreu um erro inesperado na requisição: {}'.format(err))
            raise err
        except requests.exceptions.Timeout:
            self.logger.error('Timeout. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_inventory_status(key, value)
        except requests.exceptions.ConnectionError:
            self.logger.error('Max retries exceeded. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_inventory_status(key, value)
        except Exception as err:
            self.logger.error('Ocorreu um erro inesperado: {}'.format(err))
            raise err

    def get_presence_status(self, key, value, retry=None):
        id = '{}:{}'.format(key, value)
        if retry:
            cert = config('VIVO_CERT_SP')
            cert_key = config('VIVO_KEY_SP')
        else:
            cert = config('VIVO_CERT_PB')
            cert_key = config('VIVO_KEY_PB')

        try:
            self.logger.info('Consultando em /services/REST/GlobalM2M/Inventory/v6/r12/sim/{}/presence.'
                             .format(id))
            response = requests.get(
                '{base_url}/services/REST/GlobalM2M/Inventory/v6/r12/sim/{id}/presence'.format(
                    base_url=config('VIVO_BASE_URL'), id=id),
                cert=(cert, cert_key),
                timeout=180)
            response.raise_for_status()
            response = response.json()

            return response['presenceDetailData']
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 403:
                if retry:
                    self.logger.error('Certificado e chave inválida: {}'.format(err))
                    raise err
                else:
                    self.logger.info('Contrato não pertence à Link PB.')
                    return self.get_inventory_status(key, value, True)
            elif err.response.status_code == 404:
                self.logger.error('O {} especificado não foi localizado: {}'.format(key, id))
                raise err

            self.logger.error('Ocorreu um erro inesperado na requisição: {}'.format(err))
            raise err
        except requests.exceptions.Timeout:
            self.logger.error('Timeout. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_inventory_status(key, value)
        except requests.exceptions.ConnectionError:
            self.logger.error('Max retries exceeded. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_inventory_status(key, value)
        except Exception as err:
            self.logger.error('Ocorreu um erro inesperado: {}'.format(err))
            raise err
