# -*- coding: utf-8 -*-
import logging
import requests
import time

from prettyconf import config


class Claro:
    def __init__(self, logfile_location):
        self.logger = self.conf_logger(logfile_location)

    @staticmethod
    def conf_logger(logfile_location):
        # create logger with 'Claro'
        logger = logging.getLogger('Claro')
        logger.setLevel('DEBUG')
        # create file handler which logs even info messages
        fh = logging.FileHandler(logfile_location)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        # add the handlers to the logger
        logger.addHandler(fh)
        return logger

    def get_device_details(self, iccid):
        try:
            self.logger.info('Consultando em /devices/{}.'.format(iccid))
            response = requests.get(config('CLARO_BASE_URL') + '/devices/{iccid}'.format(iccid=iccid),
                                    timeout=180,
                                    auth=(config('CLARO_USER'), config('CLARO_API_KEY')))
            response.raise_for_status()
            response = response.json()

            return response
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 401:
                self.logger.error('As credenciais de API são inválidas: {}'.format(err))
                raise err
            elif err.response.status_code == 404:
                self.logger.error('O ICCID especificado não foi localizado: {}'.format(iccid))
                raise err

            self.logger.error('Ocorreu um erro inesperado na requisição: {}'.format(err))
            raise err
        except requests.exceptions.Timeout:
            self.logger.error('Timeout. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_device_details(iccid)
        except requests.exceptions.ConnectionError:
            self.logger.error('Max retries exceeded. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_device_details(iccid)
        except Exception as err:
            self.logger.error('Ocorreu um erro inesperado: {}'.format(err))
            raise err

    def get_device_last_session(self, iccid):
        try:
            self.logger.info('Consultando em /devices/{}/sessionInfo.'.format(iccid))
            response = requests.get(config('CLARO_BASE_URL') + '/devices/{iccid}/sessionInfo'.format(iccid=iccid),
                                    timeout=180,
                                    auth=(config('CLARO_USER'), config('CLARO_API_KEY')))
            response.raise_for_status()
            response = response.json()

            return response
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 401:
                self.logger.error('As credenciais de API são inválidas: {}'.format(err))
                raise err
            elif err.response.status_code == 404:
                self.logger.error('O ICCID especificado não pode ser encontrado: {}'.format(iccid))
                raise err

            self.logger.error('Ocorreu um erro inesperado na requisição: {}'.format(err))
            raise err
        except requests.exceptions.Timeout:
            self.logger.error('Timeout. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_device_details(iccid)
        except requests.exceptions.ConnectionError:
            self.logger.error('Max retries exceeded. Aguardando 30s para tentar novamente.')
            time.sleep(30)
            return self.get_device_details(iccid)
        except Exception as err:
            self.logger.error('Ocorreu um erro inesperado: {}'.format(err))
            raise err
